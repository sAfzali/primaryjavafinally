package basic.java.oopc;

public class CreateHouse {
    public static void main(String[] args) {
        houseModelUsing();
    }

    private static void houseModelUsing() {
        HouseModel myHouse = new HouseModel("Tehran", "Alireza");
        myHouse.height = 100;
        myHouse.width = 110;
        int price = myHouse.getPrice();

        HouseModel yourHouse = new HouseModel(100, 110);
    }
}
