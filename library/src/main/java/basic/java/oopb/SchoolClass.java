package basic.java.oopb;

public class SchoolClass {
    public static void main(String[] args) {
        studentModelUsing();
    }

    private static void studentModelUsing() {
        StudentModel std = new StudentModel();
        std.name = "Alireza";
        std.family = "Mohammadi";
        std.mathScore = 18;
        std.sportScore = 12;
        std.phisicsScore = 16;

        float avr = std.getAverage();
        System.out.println("Alireza average IS " + avr);
//        System.out.println("Average IS" + std.getAverage());

        StudentModel amir = new StudentModel();
        amir.name = "Amir";
        amir.family = "Hassani";
        amir.mathScore = 12;
        amir.sportScore = 20;
        amir.phisicsScore = 8;

        float amirAvr = amir.getAverage();
        System.out.println("Amir average IS " + amir.getAverage());
//        System.out.println("Amir average IS " + amirAvr);


    }
}
