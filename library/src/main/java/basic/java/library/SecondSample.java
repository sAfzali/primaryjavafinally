package basic.java.library;

public class SecondSample {
    private String city = "Tehran";
    private int age = 27;
    private boolean isFromIran = true;
    private double pNumber = 3.14444444;
    private String country;

    private void setValue() {
        country = "Iran";
        System.out.println(country);
    }

    public static void main(String[] args) {
        System.out.println(getMyName());
    }

    private static String getMyName() {
        return "Soheil afzali";
    }

}
