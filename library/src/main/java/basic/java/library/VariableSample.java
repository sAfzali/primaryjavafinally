package basic.java.library;

public class VariableSample {
    private String name = "Soheil";
    private String family = "Afzali";
    private int age = 27;
    private boolean isIranian = true;
    private boolean iAmLive = true;
    private double pNumber = 3.144444444444;
    //default decimal number is double to Java
    // add f at the end of double to convert to float
    private float latitute = 35.242343f;

    private String getMyName() {
        return "Soheil";
    }

    private boolean isAlive() {
//        return true;
        return iAmLive;
    }

    private int getUserAge(int userID) {
        if (userID == 10) {
            return age = 36;
        } else
            return userID + 30;

    }

    private void getUser() {
        int user = getUserAge(10 + 10);
    }

}
