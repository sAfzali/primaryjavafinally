package basic.java.library;

public class CompareVariableSample {

    private void equalsForIntVariables() {
        int myAge = 27;
        int yourAge = 24;
        if (myAge == yourAge)
            print("Equals");
        else if (myAge > yourAge)
            print("I am greator");
        else
            print("You are greator");
    }

    private void equalsForBooleanVariables() {
        boolean isIranian = true;
//        if (isIranian==true)
        if (isIranian) {
            print("True");
        }
        if (!isIranian) {
            print("False");
        }
    }


    private void equalsForStringVariables() {
        String myCity = "Tehran";
        String yourCity = "Tabriz";
        if (myCity.equals(yourCity)) {
            print("Equals");
        } else
            print("NOT equals");

        String aCountry = "Iran";
        String bCountry = "iran";
        if (aCountry.equals(bCountry))
            print("Equals false");
        //False

        if (aCountry.equalsIgnoreCase(bCountry))
            print("Equals true");
        //True

        //For String Variables Length
        int myCountryLength = aCountry.length();

        if (aCountry.toLowerCase().equals(bCountry.toLowerCase()))
            print("True");
        if (aCountry.toUpperCase().equals(bCountry.toUpperCase()))
            print("True");


    }

    private static void print(String msg) {
        System.out.println(msg);
    }

}
