package basic.java.library;

public class SampleC {
    private static int year = 1398;
    private static int price = 30000;
    private static String address = "SarAfraz, beheshti";
    private static boolean expensive = true;


    public static void main(String[] args) {
//    System.out.println("This year is " + year);
        printYear(2018);
        printYear(2019);
        print("My name is Soheil");
    }

    private static void printYear(int year) {
//    System.out.println();
        print("This year is " + year);
    }

    private static void print(String msg) {
        System.out.println(msg);
    }
}
