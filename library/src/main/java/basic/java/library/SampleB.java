package basic.java.library;

public class SampleB {
    private static String name = "Soheil";
    private static String family = "Afzali";
    private static int age = 27;

    public static void main(String[] args) {
        printMyName();
    }

    //void function for non return
    private static void printMyName() {
        //print for without line to print
        System.out.print(name);
        System.out.print(family);
        //println for add line to print
        System.out.println(name);
        System.out.println(family);
        //add + for concat to String
        System.out.println("My name is " + " " + name
                + " " + "my family is " + family
                + " " + "My age is " + age);
    }
}
