package basic.java.library;

public class SwitchCaseSample {

    public static void main(String[] args) {
        getTypeByAge(20);
    }


    private static void getTypeByAge(int age) {
        switch (age) {
            case 10:
                print("Child");
                break;
            case 15:
//            case 16:
//            case 17:
                print("TeenAge");
                break;
            case 20:
                print("Young");
                break;
            case 30:
                print("Old");
                break;
            default:
                print("Unknown");
        }
    }

    private static void print(String msg) {
        System.out.println(msg);
    }


}
