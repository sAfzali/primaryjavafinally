package basic.java.library;

public class LoopSample {
    public static void main(String[] args) {

    }

    private void loopWhileSample(int number) {
        number = 1;
        while (number <= 50) {
            System.out.println(number);
            number++;
        }
    }

    private void loopDoWhileSample(int number) {
        number = 1;
        do {
            System.out.println("Number IS : " + number);
        } while (number <= 50);
    }

    private void loopForSample() {
        for (int i = 0; i < 45; i++) {
            //for (int i = 0 ; i <45; i+=2)
            //for (int i = 100 ; i <45; i-=2)
            System.out.println("I is : " + i);
        }
    }
}
