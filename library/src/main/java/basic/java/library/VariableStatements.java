package basic.java.library;

public class VariableStatements {
    //PascalCase   SoheilAfzali class names
    //camelCase    soheilAfzali function and variables names

    public static void main(String[] args) {
        getTypeByAge(80);
    }

    private static String getTypeByAge(int age) {
        if (age <= 10) {
//            return "Child";
            print("Child");
        }
        if (age > 10 && age <= 19) {
//           return "TeenAge";
            print("TeenAge");
        }
        if (age > 19 && age <= 40) {
//            return "Young";
            print("Young");
        }
        if (age > 40) {
//           return "Old";
            print("Old");
        }
        return "Unknown";
    }

    private static int getPriceByCarName(String car) {
        if (car == "pride") {
            return 20000000;
        } else
            return 0;
    }

    private static boolean isInIran(String city) {
        if (city == "Tehran") {
            return true;
        } else if (city == "Dubai") {
            return false;
        } else if (city == "tabriz") {
            return true;
        } else
            return false;
    }

    private static void print(String msg) {
        System.out.println(msg);
    }

}
