package basic.java.abstraction;

public abstract class CarMap {

    public abstract void setName();

    public abstract void setPrice();

    public abstract void getCompanyName();

}
