package basic.java.oop;

public class OopUsingA {
    public static void main(String[] args) {
        HumanPojo ali = new HumanPojo();
        ali.age = 30;
        ali.city = "Tehran";
        ali.name = "Alireza";
        System.out.println("My name is " + ali.name +
                " I am from " + ali.city + " I am " + ali.age);
    }
}
