package basic.java.arrays;

import java.util.ArrayList;
import java.util.List;

public class ArraySampleB {
    public static void main(String[] args) {
        arraySample();
        getByMaxNumberInArrayList();
    }


    private static void arraySample() {
        List<String> names = new ArrayList<>();
        names.add("Ali");
        names.add("Soheil");
        names.add("Maryam");
        names.add("Mostafa");

        for (int i = 0; i < names.size(); i++) {
            print("i : " + names.get(i));
        }

        int j = 0;
        while (j < names.size()) {
            print("j : " + names.get(j));
            j++;
        }


        for (String name : names) {
            print(name);
        }


    }

    private static void getByMaxNumberInArrayList() {
        List<Integer> numbers = new ArrayList<>();
        numbers.add(12);
        numbers.add(50);
        numbers.add(32);
        numbers.add(22);
        numbers.add(5);
        numbers.add(1);

        int max = 0;
        for (int i = 0; i < numbers.size(); i++) {
            if (max <= numbers.get(i)) {
                max = numbers.get(i);
            }
        }
        print("Max is :" + max);
    }


    private static void print(String msg) {
        System.out.println(msg);
    }
}
