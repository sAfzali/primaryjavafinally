package basic.java.arrays;

import java.util.ArrayList;
import java.util.List;

import basic.java.staticssample.StaticSample;

public class ArraySample {
    private static String name = "Ali";
    private static String family = "Hassani";
    private static String city = "Tehran";

    public static void main(String[] args) {
        arraysSample();
    }

    private static void arraysSample() {
        //Array list for String
        List<String> users = new ArrayList<>();
        //List<StaticSample> samples = new ArrayList<>();
        users.add("Soheil");
        users.add(family);
        users.add(name);

        //Array list for Integer
        List<Integer> prices = new ArrayList<>();
        prices.add(30000);
        prices.add(123456);
        prices.add(250000);
        prices.add(343000);
        prices.add(78000);
        prices.add(29990000);

        int priceSize = prices.size();
//        prices.get(1);
//        prices.size();
//        prices.add(1);
//        prices.remove(1);
        print(prices.get(2));

    }

    private static void print(String msg) {
        System.out.println("Str: " + msg);
    }

    private static void print(int msg) {
        System.out.println("Number: " + msg);
    }
}
