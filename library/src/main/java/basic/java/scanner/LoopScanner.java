package basic.java.scanner;

import java.util.Scanner;

public class LoopScanner {

    public static void main(String[] args) {
        loopForGetName();
    }


    private static void loopForGetName() {
        String name = " ";

        while (!name.equalsIgnoreCase("finish")) {
            name = getName();
        }
    }

    private static String getName() {
        Scanner scanner = new Scanner(System.in);
        print("Enter your name");
        String name = scanner.next();
        print("Name IS : " + name);
        return name;
    }


    private static void print(String msg) {
        System.out.println(msg);
    }
}
