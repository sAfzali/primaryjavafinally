package basic.java.scanner;

import java.util.Scanner;

public class CalculatorSample {
    public static void main(String[] args) {
        enterScanner();
    }

    private static void enterScanner() {
        Scanner scanner = new Scanner(System.in);
        print("Enter your first number");
        double firstNumber = scanner.nextDouble();
        print("Enter action");
        String action = scanner.next();
        print("Enter second number");
        double secondNumber = scanner.nextDouble();

        double result =
                calculator(firstNumber, secondNumber, action);
        print("Result is " + result);

    }

    private static double calculator(double first, double second, String action) {
        double result = 0;
        switch (action) {
            case "+":
                result = first + second;
                break;
            case "-":
                result = first - second;
                break;
            case "*":
                result = first * second;
                break;
            case "/":
                result = first / second;
                break;
        }
        return result;
    }


    private static void print(String msg) {
        System.out.println(msg);
    }
}
