package basic.java.scanner;

import java.util.Scanner;

public class ScannerSample {
    public static void main(String[] args) {
        scannerSample();
    }

    private static void scannerSample() {
        Scanner scanner = new Scanner(System.in);
        print("Please enter your name");
        String name = scanner.next();
        print("Please enter your age");
        int age = scanner.nextInt();
        print("My name is " + name
                + " My age is " + age);

    }


    private static void print(String msg) {
        System.out.println(msg);
    }

}
