package basic.java.loopcalculator;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Calculator {

    public static void main(String[] args) {
        getScannerAndCalculate();
    }


    private static void getScannerAndCalculate() {
        Scanner scanner = new Scanner(System.in);
        String action ="";

        List<Integer> numbers = new ArrayList<>();
        List<String> actions = new ArrayList<>();

        while (!action.equals("=")) {

            print("Enter number");
            numbers.add(scanner.nextInt());

            print("Enter Action");
            action = scanner.next();
            actions.add(action);
        }


        int result = 1;
        for (int i = 0; i < numbers.size(); i++) {
            String act = actions.get(i);
            int num = numbers.get(i);
            switch (act) {
                case "+":
                    result += i;
                    break;
                case "-":
                    result -= num;
                    break;
                case "*":
                    result *= num;
                    break;
                case "/":
                    result /= num;
                    break;
                case "=":
                    print("Result IS: " + result);
                    break;
            }
        }
    }


    private static void print(String msg) {
        System.out.println(msg);
    }
}
