package basic.java.oopd;

public class StudentClass {
    private String name;
    private String family;
    private String mobile;
    private int age;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getFamily() {
        return family;
    }

    public void setFamily(String family) {
        this.family = family;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        String trueMobile =
                mobile.replace("0", "0").
                        replace("1", "1").
                        replace("2", "2").
                        replace("3", "3").
                        replace("4", "4").
                        replace("5", "5").
                        replace("6", "6").
                        replace("7", "7").
                        replace("8", "8").
                        replace("9", "9");
        trueMobile = trueMobile.trim();
        this.mobile = trueMobile;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }
}
