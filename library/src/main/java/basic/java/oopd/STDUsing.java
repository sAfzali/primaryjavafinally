package basic.java.oopd;

public class STDUsing {
    public static void main(String[] args) {
        stdUsingClass();

    }

    private static void stdUsingClass() {
        StudentClass std = new StudentClass();
        std.setName("Soheil");
        std.setFamily("Afzali");
        std.setAge(27);

        String title = ("My name is " + std.getName()
                + " " + std.getFamily()
                + " I am " + std.getAge());
        print(title);

    }

    private static void print(String msg) {
        System.out.println(msg);
    }
}
